#include <QDBusMessage>
#include <QDBusConnection>
#include <QDebug>

int main(int argc, char** argv) {

    QDBusMessage m = QDBusMessage::createMethodCall("org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop", "org.freedesktop.portal.OpenURI", "OpenURI");
    m.setArguments({QVariant(QString()), QVariant(argv[1]), QVariant(QVariantMap{})});

    auto reply = QDBusConnection::sessionBus().call(m);

    qDebug() << reply.errorMessage();

}
